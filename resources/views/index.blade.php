@extends('layouts.principal')
@section('contenido')


    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-4 offset-lg-4 margen-login">

                <div class="card card-body card-login">

                    <div class="text-center">
                        <img class="logo-principal" src="{{ asset('imagenes/logo_google.jpg') }}" alt="Logo google">
                    </div>

                    <form>
                        <div class="form-group">
                            <label for="email">Correo electrónico</label>
                            <input type="email" class="form-control" id="email">
                        </div>
                        <div class="form-group">
                            <label for="contrasena">Contraseña</label>
                            <input type="password" class="form-control" id="contrasena">
                        </div>
                        <div class="form-group form-check">
                            <input type="checkbox" class="form-check-input" id="exampleCheck1">
                            <label class="form-check-label" for="exampleCheck1">Recordarme</label>
                        </div>
                        <button type="submit" class="btn btn-primary btn-block">Iniciar sesión</button>
                    </form>

                </div>

            </div>
        </div>
    </div>



@endsection
